from django.forms import ModelForm

from apps.models import Meet


class MeetForm(ModelForm):
    class Meta:
        model = Meet
        fields = '__all__'

    def save(self, commit=True):
        self.instance.name = self.changed_data.get('name', self.instance.name)

