from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DetailView

from apps.forms import MeetForm
from .models import Meet


# update meeting


# def post(self, request):
#     name = request.POST.get('name')
#     email = request.POST.get('email')
#     subject = request.POST.get('subject')
#     message = request.POST.get('message')
#
#     msg = f"""
#     From: {name}
#     Email: {email}
#     Massage: {subject}
#     """
#     send_mail(
#         subject=subject,
#         message=msg,
#         from_email=email,
#         recipient_list=['muhammadbilol40@gmail.com'],
#         fail_silently=True
#     )
#     return redirect('')

# view   index
def home(request):
    return render(request, 'index.html')


#  view meetings
class Create(ListView):
    model = Meet
    context_object_name = 'meetings'
    template_name = 'meetings.html'


# view  edd meeting
class Edit(CreateView):
    template_name = 'addMeeting.html'
    model = Meet
    form_class = MeetForm
    success_url = reverse_lazy('index')


class Home(CreateView):
    template_name = 'index.html'
    model = Meet
    form_class = MeetForm
    success_url = reverse_lazy('index')


#  view  search
def search(request):
    query = request.GET.get('search')
    meetings = Meet.objects.filter(name__icontains=query, )
    return render(request, 'meetings.html', {'meetings': meetings})


class MeetingDetailView(DetailView):
    template_name = 'meeting-details.html'
    model = Meet
    context_object_name = 'meeting'


class Update(UpdateView):
    template_name = 'update.html'
    model = Meet
    context_object_name = 'meeting'
    form_class = MeetForm
    success_url = reverse_lazy('add')
