from django.db import models
from modeltranslation.translator import TranslationOptions
from modeltranslation.decorators import register
from django.db import models


# Create your models here.
class Meet(models.Model):
    name = models.CharField(max_length=255)
    days = models.CharField(max_length=255)
    price = models.FloatField(max_length=20)
    description = models.TextField()
    image = models.ImageField(upload_to='images/')


    def __str__(self):
        return self.name



